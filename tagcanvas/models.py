from django.db import models
from django.utils.translation import ugettext_lazy as _

from tagcanvas.validators import HtmlColorValidator


class Cloud(models.Model):
    name = models.CharField(max_length=128)

    def __unicode__(self):
        return self.name


class Tag(models.Model):
    cloud = models.ForeignKey(Cloud)
    tag = models.CharField(_("name"), max_length=128)
    anchor = models.URLField(max_length=255, default="#")
    position = models.SmallIntegerField(null=True, blank=True)
    size = models.SmallIntegerField(null=True, blank=True)
    color = models.CharField(max_length=7, blank=True,
                             validators=[HtmlColorValidator(
                                 message='Not a valid HTML color')])

    def __unicode__(self):
        return self.tag
