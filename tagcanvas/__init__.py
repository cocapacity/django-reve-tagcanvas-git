# Copyright (c) 2009-2012 Co-Capacity.
# See LICENSE for details.

VERSION = (12, 6)
__version__ = '.'.join(map(str, VERSION))
