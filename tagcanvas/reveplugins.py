from django import forms
from django.template import RequestContext
from django.template.loader import render_to_string, get_template, Context
from django.utils.translation import ugettext_lazy as _


from contentmanager import registry
from contentmanager.models import Block, PluginType
from contentmanager.plugins import BasePlugin

from tagcanvas.models import Tag, Cloud
from tagcanvas.validators import HtmlColorValidator


class TagcanvasForm(forms.Form):
    title = forms.CharField(_("title"), required=False)
    cloud = forms.ModelChoiceField(Cloud.objects.all())


class Tagcanvas(BasePlugin):
    form = TagcanvasForm
    verbose_name = 'Tag Canvas'

    def render(self, request):
        title = self.params.get('title', u"")
        cloud = self.params.get('cloud')
        tags = Tag.objects.filter(cloud=cloud)
        context = RequestContext(request, {'title': title, 'tags': tags,
                                           "id": self.block.pk})
        return render_to_string('tagcanvas/tagcanvas.html',
                                context)

# register plugin types with Reve
registry.register(Tagcanvas)
