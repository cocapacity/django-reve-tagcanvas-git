import re

from django.core.validators import RegexValidator


class HtmlColorValidator(RegexValidator):
    regex = re.compile(r'^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$')
