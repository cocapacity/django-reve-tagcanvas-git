from tagcanvas.models import Tag, Cloud
from django.contrib import admin


class TagInline(admin.TabularInline):
    model = Tag
    extra = 1


class CloudAdmin(admin.ModelAdmin):
    inlines = [TagInline]


admin.site.register(Cloud, CloudAdmin)
admin.site.register(Tag)
