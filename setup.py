import os

from setuptools import setup, find_packages

from tagcanvas import __version__ as version


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

# Compile the list of packages available, because distutils doesn't have
# an easy way to do this.
packages, data_files = [], []
root_dir = os.path.dirname(__file__)
if root_dir:
    os.chdir(root_dir)
for dirpath, dirnames, filenames in os.walk('tagcanvas'):
    # Ignore dirnames that start with '.'
    for i, dirname in enumerate(dirnames):
        if dirname.startswith('.'):
            del dirnames[i]
    if '__init__.py' in filenames:
        pkg = dirpath.replace(os.path.sep, '.')
        if os.path.altsep:
            pkg = pkg.replace(os.path.altsep, '.')
        packages.append(pkg)
    elif filenames:
        prefix = dirpath[13:]  # Strip "tagcanvas/" or "tagcanvas\"
        for f in filenames:
            data_files.append(os.path.join(prefix, f))

# Add templates and media
data_files.extend(['templates/tagcanvas/*', 'media/tagcanvas/*'])

setup(name="tagcanvas",
      version=version,
      description="@TODO",
      long_description='\n\n'.join([read("README"), read("CHANGELOG")]),
      author='Co-Capacity',
      author_email='info@co-capacity.org',
      url='http://pypi.python.org/pypi/tagcanvas',
      package_dir={'tagcanvas': 'tagcanvas'},
      packages=packages,
      package_data={'tagcanvas': data_files},
      install_requires=['setuptools'],
      zip_safe=False,
      classifiers=[
          'Development Status :: 4 - Beta',
          'Environment :: Web Environment',
          'Framework :: Django',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: BSD License',
          'Operating System :: OS Independent',
          'Programming Language :: Python',
          'Topic :: Internet :: WWW/HTTP',
      ]
)
